# 4. Outil Fab Lab sélectionné

The following tools are available in the Fab Lab:

  * Laser cutter
  * 3D printer
  * CNC milling machine

I chose the 3D printer because it is the most accessible, versatile & fun tool.

The corresponding course taught us about repairing objects & why it is important. We learned how to use the 3D printer to make a new part for an object.

We were also introduced to [electronics components](https://en.wikipedia.org/wiki/Electronic_component) which the teacher had brought for us: resistors, capacitors, transistors, diodes, leds, potentiometers, etc. we were able to manipulate them and realise the smallness of some we had never seen in real.

We learned how to use a multimeter to measure voltage, current, resistance, etc.

We received each a box with a voluntarily misfunctioning part and we had to repair it then reinvent a problem for the next student to solve.
In case, i have placed a cardboard in my box led electric domino, "loose connection", in french, is called "false connection", it looks like it is connected but its not hehe.

We could bring a broken object but I did not have any because I always either fix it myself or give it asap to someone who can, this way, nothing is wasted, anything that asked energy to produce is either in use, being repaired or recycled.

The teacher had brought broken objects and with [Patrick Dezseri](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/patrick.dezseri/), we chose a oven knob that was broken and we decided to make a new one.

I took the measures:

![](module04/PXL_20230316_132508305.jpg)
![](module04/PXL_20230316_133413097.jpg)
![](module04/PXL_20230316_133620707.jpg)

Patrick made the 3D model on [FreeCAD](https://www.freecadweb.org/) and printed it:
![](module04/IMG_20230316_163656792_BURST000_COVER_TOP.jpg)
![](module04/IMG_20230316_163653965.jpg)

We gave the new knob to the teacher so that the broken one owner could get it from him but we never heard from him again, so
we can't tell if he is happy or not with the result but we are happy with the experience and the creation speed of 40 minutes.

More information about the course & the repair are available [on Patrick's page](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/patrick.dezseri/fabzero-modules/module04/) .
