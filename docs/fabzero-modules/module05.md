# 5. Dynamique de groupe et projet final

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Project Analysis and Design

### inspiration from fablab projects and frugal science projects:

[Paul Bryssinck's project](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/projet/finalProject-Redaction/):


#### problem tree:

| level | problem | description | 
|-------|---------|-------------|
| -2 | causes behind the causes (deeper roots) | fast fashion |
| -1 | primary problem causes (roots) | fast production model |
| 0 | main problem (trunk) |  production costs minimization "at all cost"|
| 1 | problem direct consequences (branches) | To cut production costs,  companies rely on unreasonable chimichals & low-wage workers
| 2 | secondary effects (ramifications) | environmental pollution, worker exploitation |

mermaid graph:

(if it doesn't look like a graph, check it [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/noe.bourgeois/-/blob/main/docs/fabzero-modules/module05.md))

```mermaid
graph TD;
  1[fast fashion]-->2[fast production model]-->3[production costs minimization]-->4[unreasonable chimichals]-->5[environmental pollution];
  3-->6[low-wage workers];
  6-->7[worker exploitation];
```


  


#### objective tree:

| level | problem | description |
|-------|---------|-------------|
| -1 | activities to develop to reach the objective | light triggered decoloration of the fabric |
| 0 | objective | instead of fighting against the fast fashion model, propose a new model based on the use of natural dyes and the use of local resources |
| 1 | outcomes | smarter production, less pollution, more local jobs, more sustainable fashion |

mermaid graph:

```mermaid
graph TD;
  1[instead of fighting against fast fashion model, use natural dyes & local resources based model]-->2[light triggered fabric decoloration] -->3[smarter production];
  2-->4[less pollution];
  2-->5[more local jobs];
  2-->6[more sustainable fashion];
```

## Group formation and brainstorming on project problems

To form the groups, we had to bring an object representing the problem domain 
that we wanted to improve. 
### my object:
A lamp with automatic lighting triggered by PIR sensor detection of movement in the dark.
I chose this object because ecology is perceived a priori as a sacrifice but this perfectly represents how ecology can be fun and smart and cheap(4$).
I deeply believe that settling smart ecology as desirable fashion (with a affordable price lower bound) is the key to make it mainstream.

We then had to present it to the others and explain why we wanted to improve its problem domain. 

### group:
If others had a similar problem domain, we could form a group. 

### theme:
We then had to find a common theme between the objects and then a problem that we could solve.

- ecology perceived as a sacrifice
- waste of resources


### set of problems related to possible projects

```mermaid
graph TD;
1[pollution]-->3[pollution consumption by plants];
31[ecology perceived as a sacrifice]-->32[ecological & fun smart objects]
21[waste of water]-->22[water saving];
```

## Group Dynamics Tools
Here are the tools we used to manage our group meeting dynamics and their chatGPT generated description:
### Check-in/Weather report
The group dynamic tool known as the "weather report" is a technique used to gauge the mood, atmosphere, or level of engagement within a group or team. It provides an opportunity for participants to express their current emotional state, perspective, or level of commitment at the beginning of a meeting or session.

The weather report typically involves each participant sharing a metaphor or analogy that represents their current emotional or mental state. For example, participants may use weather-related terms such as sunny, cloudy, stormy, or foggy to describe how they are feeling. The purpose is to create a safe and non-judgmental space for individuals to express themselves authentically.

The weather report technique is a simple yet effective way to kickstart a meeting, foster open communication, and set the stage for productive collaboration and problem-solving.
### Role allocation
Role allocation during a meeting is an important aspect of meeting management that can help ensure participation from all participants. Here are some common roles that can be assigned during a meeting:

#### Discussion facilitator: 
facilitates the discussion by ensuring that everyone has a chance to speak and that the discussion stays on track. They may also be responsible for summarizing the discussion and ensuring that the meeting objectives are met.

#### Secretary: 
takes notes during the meeting and shares them with the group afterward. They may also be responsible for sending out meeting reminders and keeping track of action items.

#### Timekeeper: 
alerts participants when the allocated time for a discussion has elapsed. They may also be responsible for keeping track of the time remaining for each agenda item.



### Decisions
#### Consensus: 
we used the consensus decision-making method to make general decisions. This method involves reaching a decision that everyone in the group can agree on. It is a collaborative process that encourages group members to share their opinions and ideas. It also helps to ensure that everyone feels heard and valued.

#### Majority vote:
we used the voting method to make decisions that were not unanimous. This method involves voting on a decision and then implementing the decision that receives the most votes. It is a simple and effective way to make decisions when there is no clear consensus.

The page of the group project can be found  [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/groups/group-03/-/tree/main/docs).