# 3. Impression 3D

This week I worked on making a compliant mechanism 3d model

## Classmates FlexLinks fetching

I fetched the [model](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/eliot.niedercorn/-/blob/main/docs/fabzero-modules/files/Module3/FlexCatapulte.stl) code of [Eliot Niedercorn](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/eliot.niedercorn/) and the [model](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/virginie.louckx/-/blob/main/docs/fabzero-modules/files/Flexlink_3_bon.stl) code of [Virgirnie Louckx](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/virginie.louckx/)

As their models were already LEGO dimensions compatible, I didn't have to modify their code.On the last day before the delivery, you probably know what kind of supreme relief this discovery can trigger. Special thanks to [Kamel Sanou](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kamel.sanou/) for guiding me towards Eliot's work.

## Test of the design rules for your 3D printer(s)
You want to want to know how your printer units correspond to real world ones so you get your printed model at the size you want.
Mine's rules were great & I was lucky (& helped by the lego library) so I got a good enough size at the first try.

### 3D print your flexible construction kit, make a compliant mechanism out of it with several hinges that do something
Once your model is ready, you can follow this [3d printing tutorial](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) to compile your .stl or .obj file to G-code and print it.

To see the stl model, click [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/noe.bourgeois/-/blob/main/docs/fabzero-modules/module02/twisted-heart-compression-fast-elastic-recovery.stl)

![](module03/THCFER/image/PXL_20230315_102342198.jpg)
![](module03/THCFER/image/PXL_20230315_102409525.MP.jpg)
![](module03/THCFER/image/PXL_20230315_102444425.MP.jpg)

### (compressed <10 Mo) video of my working mechanism.
As predicted by Denis, my prototype broke at the blades bases because they were too hard and thin.
 I will modify the minkowskinusoid model to empasize its join tip thickness & length. 

video:

(if it doesn't look like a video, check it [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/noe.bourgeois/-/blob/main/docs/fabzero-modules/module03/THCFER/video/PXL_20230416_100109160.mp4))

 ![](module03/THCFER/video/PXL_20230416_100109160.mp4)

## Licenses

[documentation](https://creativecommons.org/about/cclicenses/)

I didn't license my work (because I heard many stories about people open-sourcing their work and realising later they could have made billions from their ideas, I have many ideas, I accordingly want many billions. If someone wants to use my work without paying me, he can, but only after he asked me, it forces contact creation, eventually leading to group work, which independant individuals lack a lot compared to employed corporations ), except the one using the lego library, for which I repeated the author license.
## other impressions

### Belkin stand
mermaid graph:

(if it doesn't look like a graph, check it [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/noe.bourgeois/-/blob/main/docs/fabzero-modules/module03.md))

```mermaid
graph TD;
  1[My Google Pixel3]-->2[USB-C does not work anymore]-->4[I charge it with Belkin stand]

  1[My Google Pixel3]-->3[is Qi wireless power chargeable]-->4[I charge it with Belkin induction charger]-->5[When I wake up, my phone is discharged]

  3[is Qi wireless power chargeable]-->6[must be perfectly aligned with charger induction coils to charge]-->5[When I wake up, my phone is discharged]-->7[I never wake up]

  8[I use my phone to wake me up]-->7[I never wake up]-->9[I 3d-print a charger stand to force my  phone to be aligned]-->10[I can now see from my bed that it is 00:90, I still have 270 minutes of sleep]
  ;
```
![](module03/BelkinStand/PXL_20230327_120032118.MP.jpg)

[Belkin_Fast_Wireless_Charger_Stand](https://www.thingiverse.com/thing:3609898)

If you didn't yet see what you want literally emerging from the ground ( of the printer plateform) right before your eyes, you probably still don't know this superpower-like feeling, I can assure you it is very cool, watch this: 

video: 

(if it doesn't look like a video, check it [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/noe.bourgeois/-/blob/main/docs/fabzero-modules/module03/BelkinStand/PXL_20230316_154444863.LS.mp4))

![](module03/BelkinStand/PXL_20230316_154444863.LS.mp4).
