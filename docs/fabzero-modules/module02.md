# 2. Conception Assistée par Ordinateur (CAO)

This week I worked on the computer assisted design of a compliant mechanism.

## Program
I had some experience with 3D modeling softwares, like Blender, Sketchup & ThinkerCAD but I never used them for the purpose of making parametric models.

We were introduced to the open source software OpenSCAD, which allows you to create 3D models using a programming language.

## Design

### Documentation
To create LEGO bricks and pieces with openSCAD, I found on Github a great library [here](https://github.com/cfinke/LEGO.scad).


Thanks to its already normalised dimensions, we worked on making parametric FlexLinks construction kits fully compatible with each other.

### Model
We were introduced to CAD trough the heart symbol and its symmetricity and curvature properties.
I imagined a heart-shaped flexlinks construction kit, with a hole in the middle to make it wearable the bottom tip being joint by magnets ( "lovers" in french ), separable by the upper tip  cutter-like top-down push. The heart shape induced movement make the arcs align parallel to each other on the other side of the upper tip and the magnets rejoin in a "closed" position.

First, I searched in the LEGO library for the piece with the interaction maximum potential & chose the block 1x8 with the holes in it.  
![](module02/holy-block.png)  
[holy-block.scad](module02/holy-block.scad)

Then I needed to join the flexlinks blades to them in a way that would allow them to move freely without breaking.

For that, I used a scad integrated function to make a cube with curved edges, called minkowski.
![](module02/minkowski.png)  
[minkowski.scad](module02/minkowski.scad)

And added a sinusoid-like extrusion to make the flexlinks support, and called it minkowskinusoid.
![](module02/minkowskinusoid.png)  
[minkowskinusoid.scad](module02/minkowskinusoid.scad)

Then I used the minkowskinusoid at each longitudinal extremity of the flexlinks blades.

I finally carved the biggest minkowskinusoid to reduce printing time and incidentally offer a welcome space for the finger.
![](module02/2023-10-03.png)  
[twisted-heart-compression-fast-elastic-recovery.scad](module02/twisted-heart-compression-fast-elastic-recovery.scad)

I had to make adjustments accounting for  
- material properties: PLA is more rigid than I thought, so I refined the flexlinks blades to make it more flexible.
 - machine characteristics : Prusa Slicer does not add MK3S as gcode printer default parameter. 

[comment]: # (TODO: Add more information here video, etc.)
