# 1. Gestion de projet et documentation

This week I started to getting used to the documentation process.
Its purpose is to share our personal experience & difficulties.
As I already had some experience in informatics, websites, markdown and git, I had no problem I could have documented.
As a ergonomy maximalist, I mostly don't use the terminal manually when there are great Graphical User Interfaces.

We learned to compress an image to minimize its impact, I would like to add to this idea that the first problem of the internet overwhelmed by data is that an information which can be referenced by a link is instead duplicated. 

I am then careful to follow these steps:

- search if documentation already exists

- if yes, reference it

- if an improvement is needed, prefer improving the source

##  Website and how I did it
I modified the template of the class website to make it my own. 

## Introduction of myself
I added a picture of myself, a short description of my background and my previous work. I also added a link to my websites.

##  Uploading files to the archive
From
### gitlab : 

[git en francais](https://www.youtube.com/watch?v=q5E-scBPYFA&list=PLn6POgpklwWrRoZZXv0xf71mvT4E0QDOF)

If what I want to add a an idea, a short content for which the addition is not worth the run of an editor with creation support, I can use the web editor of gitlab:

[web editor doc](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#uploading-files)
 the upload is then done by a click on the "commit changes" button:
 ![](module01/webeditor_commit.png)


### VSCode :
If I used the VSCode editor, which is a very powerful tool, with a lot of extensions, and a lot of features, I followed these steps:
1. Save the file:
with command : ctrl+s
2. Commit and push the changes:
![](module01/commit_&_push.png)

### SSH key
I used a SSH key to push the changes without having to insert a password, documentation on how to generate a SSH key
can be found here:
[gitlab doc](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair)


## Compressing images and keeping storage space low

To reduce the image resolution, I did the following:

(On Windows : Get Paint.)

From chatGPT:
  
```
Open the image file that you want to reduce in size in Paint. You can do this by right-clicking on the image file and selecting "Open with" > "Paint" from the context menu.

In the Paint window, click on the "Resize" button in the "Image" group on the Home tab.

In the "Resize and Skew" window that appears, select the "Pixels" option under "Resize" and enter a new value for the horizontal and vertical resolution. For example, if the original image resolution is 300 DPI (dots per inch) and you want to reduce it to 72 DPI, you can enter "72" in both fields.

Make sure that the "Maintain aspect ratio" checkbox is checked to ensure that the aspect ratio of the image is preserved.

Click "OK" to apply the changes. The resized image will be saved as a new file with the same name and a "(1)" suffix added to the end. 
```	

We were introduced to ImageMagick, a command line tool that allows you to resize images.
It is very powerful and can be used to do a lot of things with images.
Using this tool command interface, I made an extension allowing to process images by navigating in a folder and its subfolders recursively.
It is available 
[here](https://github.com/nobourge/jpg-compress-convert-recursive).

Though, to compress one image at a time, at distant periods of time, ImageMagick is a bit overkill. And I hate command line interfaces and their unmemorable, cryptic and rigid syntax. 

## project management principles

I am going to use the project management principles by defining the following points:

- [ ] 1. scope 

- [ ] 2. goals

- [ ] 3. deliverables

- [ ] 4. constraints

- [ ] 5. assumptions

- [ ] 6. risks

- [ ] 7. team

- [ ] 8. schedule

- [ ] 9. budget

- [ ] 10.  plan

- [ ] 10.0.  communication 

- [ ] 10.1.  quality plan

- [ ] 10.2.  procurement plan

- [ ] 10.3.  stakeholder management plan

- [ ] 10.4.  acceptance plan

- [ ] 10.5.  closure plan


[comment]: # (assignment done)
