// Define the dimensions of the objects


module minkowskinusoid(width
,height
,blade_thickness
,blade_length){
    
    cube_width = width;
cube_depth = width;
cube_height = height;

//cylinder_radius = width/sqrt(2);
cylinder_radius = blade_length+3;
cylinder_height = height;
resolution = width*200;
    
    // Subtract the cylinders from the cube
    //blade_cylinder_translation_x=width*blade_length/5;
    blade_cylinder_translation_x=blade_length;
    blade_cylinder_translation_y=blade_thickness+blade_length/1.1;
    difference() {
        translate([0,-2,0])
        //translate([0,0,0])

        cube([blade_length
        , cube_depth+4
        , cube_height]);
        
        translate([blade_cylinder_translation_x
        ,0-blade_cylinder_translation_y
        ,-height/10])
        
        cylinder(r=cylinder_radius+blade_thickness/2
        , h=cylinder_height+height/5
        , $fn = resolution);
        
//        cube([cube_width
//        , cube_depth
//        , cube_height]);
//        
        translate([blade_cylinder_translation_x
        ,width+blade_cylinder_translation_y
        ,-height/10])
        
        cylinder(r=cylinder_radius+blade_thickness/2
        , h=cylinder_height+height/5
        , $fn = resolution);
    }

    translate([-width
    ,0
    ,0])

    minkowski(){
        cube([cube_width
        , cube_depth
        , cube_height-height/2]);
        
        cylinder(r=width/2
        , h=height/2
        , $fn = resolution);
    }
}

minkowskinusoid(width=10
,height=20
,blade_thickness=2
,blade_length=20);