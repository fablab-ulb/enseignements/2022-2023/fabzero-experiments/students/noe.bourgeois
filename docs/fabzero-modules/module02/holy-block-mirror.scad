 use <LEGO.scad>;
 use <minkowskinusoid.scad>;

// Define a module for the original half
module original_half() {
 
 rotate([0, 0, 180]) union() {

   color( "yellow" ) place(1, 0, 0) uncenter(1, 8) block(
            width=1,
            length=8,
            horizontal_holes=true
        );
             translate([65,10,0])

   minkowskinusoid(width=4,height=9.5);
 }}

// Call the module to create the original half
original_half();

// Use the mirror() function to create the symmetric half
module symmetric_half() {
    mirror([0,1,0]) {
        original_half();
    }
}

// Call the module to create the symmetric half
symmetric_half();

// Combine the two halves to create the full symmetric object
module symmetric_object() {
    original_half();
    symmetric_half();
}

// Call the module to create the full symmetric object
symmetric_object();
