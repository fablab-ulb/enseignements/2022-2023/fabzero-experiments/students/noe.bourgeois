minkowski(){
    side=10;
    cube(side, center=true);
    sphere(r=side/sqrt(2));
}
