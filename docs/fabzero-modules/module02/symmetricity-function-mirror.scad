// Define the dimensions of the original half
length = 20;
width = 10;
height = 5;

// Define a module for the original half
module original_half() {
    cube([length/2, width, height]);
}

// Call the module to create the original half
original_half();

// Use the mirror() function to create the symmetric half
module symmetric_half() {
    mirror([1,0,0]) {
        original_half();
    }
}

// Call the module to create the symmetric half
symmetric_half();

// Combine the two halves to create the full symmetric object
module symmetric_object() {
    original_half();
    symmetric_half();
}

// Call the module to create the full symmetric object
symmetric_object();
