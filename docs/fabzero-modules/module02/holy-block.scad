 use <LEGO.scad>;
 
 rotate([0, 0, 180]) union() {

   color( "yellow" ) place(0, 0, 0) uncenter(1, 8) block(
            width=1,
            length=8,
            horizontal_holes=true
        );
 }