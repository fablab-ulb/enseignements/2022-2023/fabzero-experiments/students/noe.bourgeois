/*
* "FlexCatapult"
* Copyright (c) [2023], Eliot Niedercorn
*
* Ce projet est sous licence Creative Commons Attribution-ShareAlike [Attribution-ShareAlike 4.0 International]
* Pour voir une copie de cette licence, visitez https://creativecommons.org/licenses/by-sa/4.0/legalcode
*/



// General Parameters
$fn = 100;
hole_radius = 2.45;
hole_diameter = hole_radius * 2;
hole_separation = 3.2;
rounding_power = 0.5;  // radius of the cylinder use for rounding my shape with minkowski()



// Catapult Part
catapult_num_holes = 3;
catapult_length = (catapult_num_holes) * (hole_diameter + hole_separation) + hole_separation;
catapult_width = 15;
catapult_height = 4;
catapult_size = [catapult_length, catapult_width, catapult_height];


module catapult(catapult_size, rounding_power){
    minkowski(){
        cube([catapult_length, catapult_width, catapult_height / 2]);  // Division per 2 to prevent minkowski() from changing the cube size
        cylinder(r = rounding_power, h = catapult_height / 2);
    }
}


module catapult_holes(catapult_size, catapult_num_holes, hole_radius, hole_separation){
    translate([hole_separation + hole_radius, catapult_size[1] / 2, -1])
    for (i = [0:catapult_num_holes-1]){
        translate([i * (2 * hole_radius + hole_separation), 0, 0])
        cylinder(r = hole_radius, h = catapult_size[2] + 3);
    }
}

difference () {
    catapult(catapult_size, rounding_power);
    catapult_holes(catapult_size, catapult_num_holes, hole_radius, hole_separation);
}



// Flexible Part
flexible_length = 100;
flexible_width = catapult_width / 6;
flexible_height = catapult_height;
flexible_size = [flexible_length, flexible_width, flexible_height];

module flexible_part(catapult_size, flexible_size) {
    translate([catapult_size[0], catapult_size[1]/2 - flexible_width/2, 0])
    cube(flexible_size);
}

flexible_part(catapult_size, flexible_size);



// Base Part
base_length = 2 * (hole_radius + hole_separation);
base_width = 10;
base_height = catapult_height / 2;
base_size = [base_length, base_width, base_height];


module base_part(catapult_size, flexible_size, base_size){
    width_translation = (base_size[1] - catapult_size[1]) / 2;
    translate([catapult_size[0] + flexible_size[0], - width_translation, 0])
    cube(base_size);
}



// Support Part
angle = 20;
support_num_holes = 3;
support_length = base_length + 2;
support_width = (catapult_num_holes) * (hole_diameter + hole_separation) + hole_separation + 2;
support_height = catapult_height / 2;
support_size = [support_length, support_width, support_height];

module support_part(catapult_size, flexible_size, base_size, support_size){
    translate([catapult_size[0] + flexible_size[0], base_size[1] / 2 + base_width / 3, 0])
    rotate(angle)
    cube(support_size);
}




// Support Holes
module support_holes(catapult_size, flexible_size, base_size, support_size, support_num_holes, hole_radius, hole_separation){
    translate([catapult_size[0] + flexible_size[0] + base_size[1] / 3, base_size[0] + 2* hole_separation, -1])
    rotate(angle + 90)
    for (i = [0:support_num_holes-1]){
        translate([i * (hole_separation + 2 * hole_radius), 0, 0])
        cylinder(r = hole_radius, h = catapult_size[2] + 5);
    }
}



// Remove extra material for 3D printing
remove_scale = 1.5;
remove_length = base_length / remove_scale;
remove_width = base_width / remove_scale;
remove_height = 2 * base_height;
remove_size = [remove_length,remove_width, remove_height];

module remove_material(catapult_size, flexible_size, base_size, support_size){
    width_translation = (base_size[1] - catapult_size[1]) / 1.5;
    translate([catapult_size[0] + flexible_size[0] + base_size[0] / 8, - width_translation + base_size[0] / 10, -1])
    minkowski(){
        cube(remove_size);
        cylinder(r = rounding_power, h = catapult_height / 2);
    }
}



// Support generation
difference(){
    difference(){
        minkowski(){
            hull(){
                base_part(catapult_size, flexible_size, base_size);
                support_part(catapult_size, flexible_size, base_size, support_size); 
            }    
            cylinder(r = rounding_power, h = catapult_height / 2);
        }
        remove_material(catapult_size, flexible_size, base_size, support_size);
    }
    support_holes(catapult_size, flexible_size, base_size, support_size, support_num_holes, hole_radius, hole_separation);
}