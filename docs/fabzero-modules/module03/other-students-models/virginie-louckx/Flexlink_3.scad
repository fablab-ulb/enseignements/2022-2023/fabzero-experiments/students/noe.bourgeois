

$fn=50;
nbr_trous = 1;

height = 1.5;
middle_length = 72;
center_holes_distance = 8;
width_edge = 1;
diameter_hole = 5.1;
structure_width = diameter_hole+width_edge*2;

//modif pour barre perpendiculaire
long_barre_perpend=40;
width_barre_perpend=2;
dist_x = 0;
dist_y = 2;



width_bar=2;



x0=0;x1=0;x2=0;
z0=0; z1=0;z2=0;
 
y0=middle_length/2+center_holes_distance/2+structure_width/2; 
y1=y0+center_holes_distance/2;  
y2=y0-center_holes_distance/2;  



union(){
    
cube([width_bar,middle_length,height], center= true);
bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, center_holes_distance, structure_width, height, diameter_hole) ; 

mirror([0,1,0]) bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, center_holes_distance, structure_width, height, diameter_hole);


}


module bout(x0,y0,z0, x1,y1,z1, x2,y2,z2, center_holes_distance, structure_width, height, diameter_hole){

    difference(){
    if(nbr_trous==1){
		rectanglearrondi(x0,y0-center_holes_distance/2-0.1,z0,center_holes_distance*0, structure_width, height);
    }
        
    if (nbr_trous==2) {
		rectanglearrondi(x0,y0-0.1,z0,center_holes_distance, structure_width, height);
    }
        
        
    if (nbr_trous == 1){
		translate([x1,y0-center_holes_distance/2,z1]) cylinder(height+0.1, d= diameter_hole, center=true);
    }
    if (nbr_trous == 2){
        translate([x1,y1,z1]) cylinder(height+0.1, d= diameter_hole, center=true);
        translate([x2,y2,z2]) cylinder(height+0.1, d= diameter_hole, center=true);
    }
    }
}

module rectanglearrondi(x0,y0,z0,center_holes_distance, structure_width, height){

    union(){
    translate([x0,y0,z0]) cube([structure_width,center_holes_distance+1, height], center=true);

    translate([x0,y0-(center_holes_distance)/2,z0])cylinder(height, d=structure_width, center=true);

    translate([x0,y0+(center_holes_distance)/2,z0])cylinder(height, d=structure_width, center=true);
    }
}

barre_perpend();

module barre_perpend() {
    translate([dist_x,dist_y,0]) cube([long_barre_perpend,width_barre_perpend,height], center = true);
}







































