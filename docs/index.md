## Foreword

Hello!

This my student blog for the [2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).

The GitLab software  is set to use [MkDocs](https://www.mkdocs.org/) to turn simple text files written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, into the site you are navigating.

Each student has a personal GitLab remote repository in the [GitLab class group](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students) in which a template of a blog is already stored. 

Mine can be found [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/noe-bourgeois).



## About me
![](images/DSC_5834-Copy.JPG)

Hi! I am Noe Bourgeois. I am a graphic designer & programmer based in Bruxelles working on technology design & ergonomy.

[Visit these websites to see my work!
](https://cosoc.com/noe)

## My background

I have some experience in programming, electronics, and 3D modeling. As an aside, I'm learning graphic design 

I started to study informatics at ULB at the "ctrl-f ignorant" level but managed to not ragequit and that is a necessary and sufficient condition to hire me.

Starting these studies with a big knowledge gap triggered a lot of curiosity and a constant research & heavy use of any & every technology helping one's work.

## Previous work

### Dino Duck To The Beat

For the [ULB course of Digital Electronics](https://www.ulb.be/en/programme/elec-h310), we had a very interesting & funny project based on the [No Internet Dino game](https://trex-runner.com/) which we automatized using sensors:

[here](https://www.youtube.com/watch?v=XGvUvMnE9nQ) & [there](https://www.youtube.com/watch?v=JOFl63-6Mqc)

