# Read Me

This is my student GitLab repository of the [2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/).

# website

https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/noe.bourgeois/

# assignment
## current
https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/fabzero-modules/computer-aided-design/#:~:text=covid%20crisis.-,Assignment,work.%20Modify%20its%20code%20and%20get%20the%20parts%20ready%20to%20print.,-Creative%20Commons%20Attribution
